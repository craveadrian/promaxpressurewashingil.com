<div id="welcome">
	<div class="row">
		<div class="container">
			<div class="wlcLeft">
				<h3>Welcome To</h3>
				<h1> Promax <span>Pressure Washing</span> </h1>
				<p>Are you looking for a professional pressure washing service in Addison, IL area? Did you know there is one nearby that treats customers like family rather than clients, and also meet their needs if not exceed their expectations? Welcome to <?php $this->info("company_name"); ?>. We are a one-stop solution for all your pressure washing needs in Addison and beyond. We guarantee excellent customer service, good value for money and unmatched quality services. We are simply ideal for all your cleaning projects.</p>
				<div class="flexbox">
					<a href="about#content" class="btn">Learn more</a>
					<p>
						CALL US TODAY
						<span>
							<?php $this->info(["phone","tel"]); ?>
						</span>
					</p>
				</div>
			</div>
			<div class="wlcRight">
				<img src="public/images/content/welcome1.jpg" alt="washing truck">
			</div>
		</div>
		<div class="container2">
			<img src="public/images/content/welcome2.jpg" alt="stairs">
			<img src="public/images/content/welcome3.jpg" alt="fense">
			<img src="public/images/content/welcome4.jpg" alt="floor">
		</div>
	</div>
</div>
<div id="services">
	<div class="container">
		<div class="row">
			<div class="svcLeft">
				<img src="public/images/content/service1.jpg" alt="washing floor">
			</div>
			<div class="svcRight">
				<h2>OUR <span>SERVICES</span> </h2>
				<div class="service-container">
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite1"></a> </dt>
						<dd><a href="services#content">Siding Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite2"></a> </dt>
						<dd><a href="services#content">Deck Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite3"></a> </dt>
						<dd><a href="services#content">Patio Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite4"></a> </dt>
						<dd><a href="services#content">Roof Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite5"></a> </dt>
						<dd><a href="services#content">Concrete Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite6"></a> </dt>
						<dd><a href="services#content">Sidewalk Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite7"></a> </dt>
						<dd><a href="services#content">Commercial</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite8"></a> </dt>
						<dd><a href="services#content">Residential</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite9"></a> </dt>
						<dd><a href="services#content">Industrial</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite10"></a> </dt>
						<dd><a href="services#content">Interior Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <img src="public/images/sprites.png" alt="Image sprite" class="sprite11"></a> </dt>
						<dd><a href="services#content">Exterior Cleaning</a></dd>
					</dl>
					<dl>
						<dt> <a href="services#content"><img src="public/images/sprites.png" alt="Image sprite" class="sprite12"></a> </dt>
						<dd><a href="services#content">Iron Works</a></dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="why-choose-us">
	<div class="row">
		<div class="container">
			<h2>WHY <span>CHOOSE US?</span></h2>
			<p>Compared to other pressure washing services in the area, we’re more experienced and offer quality services thanks to several years in the business. We also have a state-of-the-art pressure washer. Our aim is to help you reach your goals and objectives promptly, and create long-lasting relationships with our clients. We have the ideal portable pressure washer that’ll meet all your pressure cleaning needs.</p>
			<p>​What tips the scales in our favor the most is that we use a portable pressure washer that provides fast results. Also, at <?php $this->info("company_name"); ?>, we provide cost-effective, convenient cleaning services. We won’t just pressure clean your home, but also your office in Addison and nearby communities including Elk Grove Village, Buffalo Grove, and Glen Ellyn. Our prices don’t change. There are no hidden charges either.</p>
			<a href="about#content" class="btn">Learn more</a>
		</div>
	</div>
</div>
<div id="recent-works">
	<div class="row">
		<h2>RECENT <span>WORKS</span></h2>
		<div class="container1">
			<img src="public/images/content/works1.jpg" alt="Work Image 1">
			<img src="public/images/content/works2.jpg" alt="Work Image 2">
			<img src="public/images/content/works3.jpg" alt="Work Image 3">
		</div>
		<div class="container2">
			<a href="gallery#content" class="btn">Learn more</a>
			<p>Our quality of service and professionalism is among the best in the area, thanks to our use of a hi-tech pressure washer, and employing the best pressure washing specialists you can find in Addison, IL. Turn to us today! We guarantee 100% customer satisfaction as well as customer services.</p>
		</div>
	</div>
</div>
<div id="book">
	<div class="row">
		<h2>BOOK NOW</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="cntTop">
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
			</div>
			<div class="cntMid">
				<label><span class="ctc-hide">Message / Questions</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
				</label>
			</div>
			<div class="cntBot">
				<div class="cntBotLeft">
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
				</div>
				<div class="cntBotRight">
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
				</div>
			</div>
			<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
		</form>
	</div>
</div>
<div id="information">
	<div class="row">
		<a href="https://www.google.com.ph/maps/place/940+IL-53+%23101a,+Itasca,+IL+60143,+USA/@41.9449186,-88.0344182,17.5z/data=!4m5!3m4!1s0x880fada0fec325ad:0x4d79e0c2c3bc9c10!8m2!3d41.9447872!4d-88.0340571"><img src="public/images/common/map.jpg" alt="map" class="map"></a>
		<div class="container">
		  <div class="infoLeft">
				<h4>GET IN TOUCH</h4>
				<a href="<?php echo URL ?>">
					<img src="public/images/common/footLogo.png" alt="<?php $this->info("company_name"); ?> Logo" class="footLogo">
				</a>
				<p class="sm">
					<a href="<?php $this->info("fb_link") ?>" class="socialico" target="_blank">f</a>
					<a href="<?php $this->info("gp_link") ?>" class="socialico" target="_blank">g</a>
					<a href="<?php $this->info("li_link") ?>" class="socialico" target="_blank">i</a>
					<a href="<?php $this->info("tt_link") ?>" class="socialico" target="_blank">l</a>
					<a href="<?php $this->info("yt_link") ?>" class="socialico" target="_blank">x</a>
				</p>
		  </div>
			<div class="infoRight">
				<p>PHONE <span><?php $this->info(["phone","tel","footPhone"]); ?></span> </p>
				<p>EMAIL <span><?php $this->info(["email","mailto"]); ?></span> </p>
				<p><span><?php $this->info("address"); ?></span> </p>
			</div>
		</div>
	</div>
</div>
